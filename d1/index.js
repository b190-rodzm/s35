const express = require("express");
const mongoose = require("mongoose");

const app = express();

mongoose.connect("mongodb+srv://mikerodz:Hydepark@wdc028-course-booking.nlohbhy.mongodb.net/b190-to-do?retryWrites=true&w=majority",
{
  useNewUrlParser: true,
  useUnifiedTopology: true
}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open",()=> console.log("We are connected to the database."));

const taskSchema = new mongoose.Schema({
  name: String,
  status:{
    type: String,
    default: "pending"
  }
});

const Task = mongoose.model("Task", taskSchema);

app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.post("/tasks",(req, res)=>{
  Task.findOne({name : req.body.name},(err,result)=>{ 
    if (result !==null && result.name === req.body.name){
      return res.send("Duplicate task found");
    } else {
      let newTask = new Task({
        name: req.body.name
      });
      newTask.save((saveErr, savedTask)=>{
        if (saveErr){
          return console.error(saveErr);
        }else{
          return res.status(201).send("New task created");
        };
      });
    };
  });
});

app.get("/tasks", (req, res) => {
  Task.find({},(err, result) => {
    if (err){
      return console.log(err);
    } else {
      return res.status(200).json(result);
    };
  });
});

// ===========================
// ACTIVITY
// ===========================

// Activity:
// 1. Create a User schema.
// 2. Create a User model.
// 3. Create a POST route that will access the "/signup" route that will create a user.
// 4. Process a POST request at the "/signup" route using postman to register a user.
// 5. Create a git repository named S35.
// 6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 7. Add the link in Boodle.

const userSchema = new mongoose.Schema({
  userName: String,
  password:{
    type: String,
  }
});

const User = mongoose.model("User", userSchema);


app.post("/signup",(req, res)=>{
  User.findOne({userName : req.body.username},(err,result)=>{ 
    if (result !==null && result.userName === req.body.username){
      return res.send("Duplicate user found");
    } else {
      let newUser = new User({
        userName: req.body.username,
        password: req.body.password
      });
      newUser.save((saveErr, savedUser)=>{
        if (saveErr){
          return console.error(saveErr);
        }else{
          return res.status(201).send("New user registered");
        };
      });
    };
  });
});


app.listen(3000, () => console.log('Server running at port 3000.'));